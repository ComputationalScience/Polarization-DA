
# Polarization DA

Notebooks for Physics of Social Systems workshop (https://www.dpg-physik.de/veranstaltungen/2021/wochenendseminar-physik-sozialer-systeme-online).

This repository is meant to run at https://mybinder.org/.

The Jupyter notebook ``polarization-da.ipynb`` implements a Bayesian MCMC routine, which we use to learn parameters of a Markov chain model that describes the evolution of political polarization in the United States. 

Here is an example of an output file:

![Image](ideology_distr_1994_public_smoothened.png)

For more information on Bayesian MCMC, see Gelman, A., Carlin, J. B., Stern, H. S., Dunson, D. B., Vehtari, A., & Rubin, D. B. (2013). Bayesian data analysis. CRC press.

## Related Data Sources

* https://www.people-press.org/2017/10/05/the-partisan-divide-on-political-values-grows-even-wider/
* https://www.pewresearch.org/methods/u-s-survey-research/frequently-asked-questions/
* https://www.pewresearch.org/methods/u-s-survey-research/

## References
* L. Böttcher, H. Gersbach, [The Great Divide: Quantifying Drivers of Polarization in the US Public](https://epjdatascience.springeropen.com/articles/10.1140/epjds/s13688-020-00249-4), EPJ Data Science 9, 32 (2020)
* L. Böttcher, P. Montealegre, E. Goles, H. Gersbach, [Competing Activists--Political Polarization](https://www.sciencedirect.com/science/article/abs/pii/S0378437119320679?via%3Dihub#GS1), Physica A 545, 123713 (2020)

Please cite our works if you use our Bayesian MCMC and data analysis frameworks.

```
@article{bottcher2020great,
  title={The great divide: drivers of polarization in the US public},
  author={B{\"o}ttcher, Lucas and Gersbach, Hans},
  journal={EPJ Data Science},
  volume={9},
  number={1},
  pages={1--13},
  year={2020},
  publisher={Springer}
}
```
```
@article{bottcher2020competing,
  title={Competing activists—political polarization},
  author={B{\"o}ttcher, Lucas and Montealegre, Pedro and Goles, Eric and Gersbach, Hans},
  journal={Physica A: Statistical Mechanics and its Applications},
  volume={545},
  pages={123713},
  year={2020},
  publisher={Elsevier}
}
```

#%%
# read in data
import numpy as np
import scipy as sp
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as md
import dateutil
import datetime
import pollster
import random
import os
import re
import pandas as pd
from scipy.stats import norm
from scipy.integrate import trapz
from matplotlib import rcParams

# customized settings
params = {  # 'backend': 'ps',
    'font.family': 'serif',
    'font.serif': 'Latin Modern Roman',
    'font.size': 10,
    'axes.labelsize': 'medium',
    'axes.titlesize': 'medium',
    'legend.fontsize': 'medium',
    'xtick.labelsize': 'small',
    'ytick.labelsize': 'small',
    'savefig.dpi': 150,
    'text.usetex': True}
# tell matplotlib about your params
rcParams.update(params)

# set nice figure sizes
fig_width_pt = 245    # Get this from LaTeX using \showthe\columnwidth
golden_mean = (np.sqrt(5.) - 1.) / 2.  # Aesthetic ratio
ratio = golden_mean
inches_per_pt = 1. / 72.27  # Convert pt to inches
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * ratio  # height in inches
fig_size = [fig_width, fig_height]
rcParams.update({'figure.figsize': fig_size})

def product(p_arr, q_arr):
    product_arr = np.zeros_like(p_arr)
    
    product_arr[0] = 1
    for i in range(len(p_arr)-1):
        product_arr[i+1] = product_arr[i]*p_arr[i]/q_arr[i+1]
                
    product_arr /= trapz(product_arr, np.linspace(-1, 1, len(product_arr)))

    return product_arr
    
#%%
def cost_function(x, data):
    
    p_arr = x[:len(x)//2]
    q_arr = x[len(x)//2:]
    
    # compute stationary solution
    stationary = product(p_arr, q_arr)
        
    # stationary distribution
    binned_stationary = 0.5*(stationary+np.roll(stationary, -1))[:-1]  
    
    # least-squares data
    phi = np.linalg.norm(binned_stationary-data)/len(data)
    
    return phi4

def cost_function_A(lambd, pqA, data):
    
    p_arr = pqA[:len(pqA)//2]
    q_arr = pqA[len(pqA)//2:]
    
    # compute stationary solution
    stationary = product(p_arr/lambd**0.5, q_arr*lambd**0.5)
    
    # stationary distribution
    binned_stationary = 0.5*(stationary+np.roll(stationary, -1))[:-1]
    
    # least-squares data
    phi = np.linalg.norm(binned_stationary-data)/len(data)
    
    return phi

def cost_function_B(lambd, pqB, data):
    
    p_arr = pqB[:len(pqB)//2]
    q_arr = pqB[len(pqB)//2:]
    
    # compute stationary solution
    stationary = product(p_arr*lambd**0.5, q_arr/lambd**0.5)

    # stationary distribution
    binned_stationary = 0.5*(stationary+np.roll(stationary, -1))[:-1]    

    # least-squares dataideology_distr_2017
    phi = np.linalg.norm(binned_stationary-data)/len(data)
    
    return phi

def initialParameters(lowerBound, upperBound):
    lambd = np.random.uniform(lowerBound, upperBound)
    return lambd  
    
def updateLambda(meanAcceptance, lambda0, Sigma, thetaArr):
    meanAcceptance /= 500
    lambda0 *= np.exp((meanAcceptance-0.44)/i)
    thetaArrSigma = np.asarray(thetaArr)[-500:]
    Sigma = 0.25*Sigma + 0.75*np.var(thetaArrSigma)
    print(i, lambda0, meanAcceptance, len(thetaArr))
    meanAcceptance = 0
    return meanAcceptance, lambda0, Sigma
    
def updataTheta(meanAcceptance, thetaArr, error, theta, thetaNew, lowerBound, upperBound, cost_function, data, pq):
    r = 0
    
    if np.logical_and(thetaNew <= upperBound, thetaNew >= lowerBound):   
        
        # model parameters    
        lambd = thetaNew
        
        errorNew = cost_function(lambd, pq, data)
        #print("error", errorNew, error, thetaNew, theta)
        r = min([np.exp(-5*(errorNew-error)/error), 1])
        
        #print(i, errorNew, error, r)
        
    if np.random.random() < r:
        theta = np.copy(thetaNew)
        thetaArr.append(thetaNew)
        error = errorNew
        meanAcceptance += 1
    else:
        thetaArr.append(theta)
        
    return meanAcceptance, thetaArr, error, theta

self_democrat = np.asarray([0.315, 0.335, 0.335, 0.323, 0.315, 0.304, 0.31])
self_republican = np.asarray([0.301, 0.266, 0.3, 0.243, 0.232, 0.237, 0.2425])

lean_democrat = np.asarray([0.122, 0.145, 0.134, 0.156, 0.165, 0.173, 0.1875])
lean_republican = np.asarray([0.137, 0.130, 0.117, 0.157, 0.162, 0.164, 0.165])

democrats_d_republicans = (self_democrat+lean_democrat)/(self_republican+lean_republican)

print(democrats_d_republicans)

years = [1994, 1999, 2004, 2011, 2014, 2015, 2017]
yearyear = years[-1]
groups = ["public"]
binwidth = 0.1

democratsPublicList = []
meanDemocratsPublic = []
stdDemocratsPublic = []

republicansPublicList = []
meanRepublicansPublic = []
stdRepublicansPublic = []

democratsEngagedList = []
meanDemocratsEngaged = []
stdDemocratsEngaged = []

republicansEngagedList = []
meanRepublicansEngaged = []
stdRepublicansEngaged = []

lambA_arr = []
lambB_arr = []

democrats_histogram = 0
democrats_bins = 0
republicans_histogram = 0
republicans_bins = 0

for group in groups:
    for year in years:
        data = np.loadtxt("DataAndSvg/%d_%s.dat"%(year,group))    
        
        democratsData = []        
        republicanData = []        

        for i in range(len(data)):
            democratsData.extend([data[i][0]]*int(round(data[i][1])))
            republicanData.extend([data[i][0]]*int(round(data[i][2])))
            
        if group == "public":    
            
            democratsPublicList.append(democratsData)
            meanDemocratsPublic.append(np.mean(democratsData))
            stdDemocratsPublic.append(np.std(democratsData))
            
            republicansPublicList.append(republicanData)
            meanRepublicansPublic.append(np.mean(republicanData))
            stdRepublicansPublic.append(np.std(republicanData))

        if group == "engaged":
            
            democratsEngagedList.append(democratsData)
            meanDemocratsEngaged.append(np.mean(democratsData))
            stdDemocratsEngaged.append(np.std(democratsData))

            republicansEngagedList.append(republicanData)
            meanRepublicansEngaged.append(np.mean(republicanData))
            stdRepublicansEngaged.append(np.std(republicanData))
            
        data = np.loadtxt('optimal_pqApqB_1994_public_smoothened.dat')
        
        pA_arr_opt = np.copy(data[:,1])
        qA_arr_opt = np.copy(data[:,2])
        pB_arr_opt = np.copy(data[:,3])
        qB_arr_opt = np.copy(data[:,4])
        
        pqA = np.append(np.copy(pA_arr_opt), np.copy(qA_arr_opt))
        pqB = np.append(np.copy(pB_arr_opt), np.copy(qB_arr_opt))

        democrats_histogram, democrats_bins = np.histogram(democratsData, bins=np.arange(-1, 1 + binwidth, binwidth), normed=True)
        republicans_histogram, republicans_bins = np.histogram(republicanData, bins=np.arange(-1, 1 + binwidth, binwidth), normed=True)
        print(year)        
        np.savetxt('histograms_%d.dat'%year, np.c_[0.5*(democrats_bins+np.roll(democrats_bins, -1))[:-1], democrats_histogram, republicans_histogram])

#%%
        # initial data fit
        upperBound = 2.0
        lowerBound = 0.5
        
        # initial model parameters
        
        thetaA1 = initialParameters(lowerBound, upperBound)
        thetaA2 = initialParameters(lowerBound, upperBound)
        thetaA3 = initialParameters(lowerBound, upperBound)
        thetaA4 = initialParameters(lowerBound, upperBound)
        
        thetaB1 = initialParameters(lowerBound, upperBound)
        thetaB2 = initialParameters(lowerBound, upperBound)
        thetaB3 = initialParameters(lowerBound, upperBound)
        thetaB4 = initialParameters(lowerBound, upperBound)
            
        theta1Arr_A = []
        theta1Arr_A.append(thetaA1)
        
        theta2Arr_A = []
        theta2Arr_A.append(thetaA2)
        
        theta3Arr_A = []
        theta3Arr_A.append(thetaA3)
        
        theta4Arr_A = []
        theta4Arr_A.append(thetaA4)
        
        theta1Arr_B = []
        theta1Arr_B.append(thetaB1)
        
        theta2Arr_B = []
        theta2Arr_B.append(thetaB2)
        
        theta3Arr_B = []
        theta3Arr_B.append(thetaB3)
        
        theta4Arr_B = []
        theta4Arr_B.append(thetaB4)
        
        error1_A = cost_function_A(thetaA1, pqA, democrats_histogram)
        error2_A = cost_function_A(thetaA2, pqA, democrats_histogram)
        error3_A = cost_function_A(thetaA3, pqA, democrats_histogram)
        error4_A = cost_function_A(thetaA4, pqA, democrats_histogram)
        
        error1_B = cost_function_B(thetaB1, pqB, republicans_histogram)
        error2_B = cost_function_B(thetaB2, pqB, republicans_histogram)
        error3_B = cost_function_B(thetaB3, pqB, republicans_histogram)
        error4_B = cost_function_B(thetaB4, pqB, republicans_histogram)
        
        meanAcceptance1_A = 0
        lambda01_A = 2.4
        Sigma1_A = 1
        errorArr1_A = []
        
        meanAcceptance2_A = 0
        lambda02_A = 2.4
        Sigma2_A = 1
        errorArr2_A = []
        
        meanAcceptance3_A = 0
        lambda03_A = 2.4
        Sigma3_A = 1
        errorArr3_A = []
        
        meanAcceptance4_A = 0
        lambda04_A = 2.4
        Sigma4_A = 1
        errorArr4_A = []
        
        meanAcceptance1_B = 0
        lambda01_B = 2.4
        Sigma1_B = 1
        errorArr1_B = []
        
        meanAcceptance2_B = 0
        lambda02_B = 2.4
        Sigma2_B = 1
        errorArr2_B = []
        
        meanAcceptance3_B = 0
        lambda03_B = 2.4
        Sigma3_B = 1
        errorArr3_B = []
        
        meanAcceptance4_B = 0
        lambda04_B = 2.4
        Sigma4_B = 1
        errorArr4_B = []
        
        notstationary_A = 1
        cnt_A = 0
        
        notstationary_B = 1
        cnt_B = 0
        
        for i in range(300000):
            
            if cnt_A >= 10000:
                break
            
            if notstationary_A == 0:
                 cnt_A += 1
                 
            if np.logical_and(i > 0, np.mod(i, 500) == 0):
                
                if notstationary_A:
                    #plt.figure()
                    #plt.plot(errorArr1_A)
                    #plt.plot(errorArr2_A)
                    #plt.plot(errorArr3_A)
                    #plt.plot(errorArr4_A)
                    #plt.show()
                    
                    #plt.figure()
                    #plt.hist(theta1Arr_A, bins=20)
                    #plt.show()
                    
                    meanAcceptance1_A, lambda01_A, Sigma1_A = updateLambda(meanAcceptance1_A, lambda01_A, Sigma1_A, theta1Arr_A)
                    meanAcceptance2_A, lambda02_A, Sigma2_A = updateLambda(meanAcceptance2_A, lambda02_A, Sigma2_A, theta2Arr_A)
                    meanAcceptance3_A, lambda03_A, Sigma3_A = updateLambda(meanAcceptance3_A, lambda03_A, Sigma3_A, theta3Arr_A)
                    meanAcceptance4_A, lambda04_A, Sigma4_A = updateLambda(meanAcceptance4_A, lambda04_A, Sigma4_A, theta4Arr_A)
                
                std1 = np.std(errorArr1_A[-5000:])
                std2 = np.std([errorArr1_A[-1], errorArr2_A[-1], errorArr3_A[-1], errorArr4_A[-1]])  
                print('std 1', std1)
                print('std 2', std2)
                print('std fraction', std1/std2)
                
                print('error', np.mean([errorArr1_A[-1], errorArr2_A[-1], errorArr3_A[-1], errorArr4_A[-1]]))
                print('notstationary', notstationary_A)
                print('len theta1Arr', len(theta1Arr_A))
        
                if notstationary_A:
                    if len(theta1Arr_A) >= 10002:
                        theta1Arr_A = []
                        theta2Arr_A = []
                        theta3Arr_A = []
                        theta4Arr_A = []
                    
                    if i > 10000:
                        if np.logical_and(std1/std2 <= 1.3, std1/std2 >= 0.7):
                            notstationary_A = 0
                            theta1Arr_A = []
                            theta2Arr_A = []
                            theta3Arr_A = []
                            theta4Arr_A = []
        
            # new theta
            #print(thetaA1, lambda01_A**2*Sigma1_A)
            thetaA1New = np.random.normal(thetaA1, np.sqrt(lambda01_A**2*Sigma1_A))
            thetaA2New = np.random.normal(thetaA2, np.sqrt(lambda02_A**2*Sigma2_A))
            thetaA3New = np.random.normal(thetaA3, np.sqrt(lambda03_A**2*Sigma3_A))
            thetaA4New = np.random.normal(thetaA4, np.sqrt(lambda04_A**2*Sigma4_A))
        
            meanAcceptance1_A, theta1Arr_A, error1_A, thetaA1 = updataTheta(meanAcceptance1_A, theta1Arr_A, error1_A, thetaA1, thetaA1New, lowerBound, upperBound, cost_function_A, democrats_histogram, pqA)
            meanAcceptance2_A, theta2Arr_A, error2_A, thetaA2 = updataTheta(meanAcceptance2_A, theta2Arr_A, error2_A, thetaA2, thetaA2New, lowerBound, upperBound, cost_function_A, democrats_histogram, pqA)
            meanAcceptance3_A, theta3Arr_A, error3_A, thetaA3 = updataTheta(meanAcceptance3_A, theta3Arr_A, error3_A, thetaA3, thetaA3New, lowerBound, upperBound, cost_function_A, democrats_histogram, pqA)
            meanAcceptance4_A, theta4Arr_A, error4_A, thetaA4 = updataTheta(meanAcceptance4_A, theta4Arr_A, error4_A, thetaA4, thetaA4New, lowerBound, upperBound, cost_function_A, democrats_histogram, pqA)
        
            errorArr1_A.append(error1_A)
            errorArr2_A.append(error2_A)
            errorArr3_A.append(error3_A)
            errorArr4_A.append(error4_A)
        
        for i in range(300000):

            if cnt_B >= 10000:
                break
            
            if notstationary_B == 0:
                 cnt_B += 1
                 
            if np.logical_and(i > 0, np.mod(i, 500) == 0):
                
                if notstationary_B:
                    #plt.figure()
                    #plt.plot(errorArr1_B)
                    #plt.plot(errorArr2_B)
                    #plt.plot(errorArr3_B)
                    #plt.plot(errorArr4_B)
                    #plt.show()
                    
                    #plt.figure()
                    #plt.hist(theta1Arr_B, bins=20)
                    #plt.show()
                    
                    meanAcceptance1_B, lambda01_B, Sigma1_B = updateLambda(meanAcceptance1_B, lambda01_B, Sigma1_B, theta1Arr_B)
                    meanAcceptance2_B, lambda02_B, Sigma2_B = updateLambda(meanAcceptance2_B, lambda02_B, Sigma2_B, theta2Arr_B)
                    meanAcceptance3_B, lambda03_B, Sigma3_B = updateLambda(meanAcceptance3_B, lambda03_B, Sigma3_B, theta3Arr_B)
                    meanAcceptance4_B, lambda04_B, Sigma4_B = updateLambda(meanAcceptance4_B, lambda04_B, Sigma4_B, theta4Arr_B)
                
                std1 = np.std(errorArr1_B[-5000:])
                std2 = np.std([errorArr1_B[-1], errorArr2_B[-1], errorArr3_B[-1], errorArr4_B[-1]])  
                print('std 1', std1)
                print('std 2', std2)
                print('std fraction', std1/std2)
                
                print('error', np.mean([errorArr1_B[-1], errorArr2_B[-1], errorArr3_B[-1], errorArr4_B[-1]]))
                print('notstationary', notstationary_B)
                print('len theta1Arr', len(theta1Arr_B))
        
                if notstationary_B:
                    if len(theta1Arr_B) >= 10002:
                        theta1Arr_B = []
                        theta2Arr_B = []
                        theta3Arr_B = []
                        theta4Arr_B = []
                    
                    if i > 10000:
                        if np.logical_and(std1/std2 <= 1.3, std1/std2 >= 0.7):
                            notstationary_B = 0
                            theta1Arr_B = []
                            theta2Arr_B = []
                            theta3Arr_B = []
                            theta4Arr_B = []
        
            # new theta
            #print(thetaA1, lambda01_A**2*Sigma1_A)
            thetaB1New = np.random.normal(thetaB1, np.sqrt(lambda01_B**2*Sigma1_B))
            thetaB2New = np.random.normal(thetaB2, np.sqrt(lambda02_B**2*Sigma2_B))
            thetaB3New = np.random.normal(thetaB3, np.sqrt(lambda03_B**2*Sigma3_B))
            thetaB4New = np.random.normal(thetaB4, np.sqrt(lambda04_B**2*Sigma4_B))
        
            meanAcceptance1_B, theta1Arr_B, error1_B, thetaB1 = updataTheta(meanAcceptance1_B, theta1Arr_B, error1_B, thetaB1, thetaB1New, lowerBound, upperBound, cost_function_B, republicans_histogram, pqB)
            meanAcceptance2_B, theta2Arr_B, error2_B, thetaB2 = updataTheta(meanAcceptance2_B, theta2Arr_B, error2_B, thetaB2, thetaB2New, lowerBound, upperBound, cost_function_B, republicans_histogram, pqB)
            meanAcceptance3_B, theta3Arr_B, error3_B, thetaB3 = updataTheta(meanAcceptance3_B, theta3Arr_B, error3_B, thetaB3, thetaB3New, lowerBound, upperBound, cost_function_B, republicans_histogram, pqB)
            meanAcceptance4_B, theta4Arr_B, error4_B, thetaB4 = updataTheta(meanAcceptance4_B, theta4Arr_B, error4_B, thetaB4, thetaB4New, lowerBound, upperBound, cost_function_B, republicans_histogram, pqB)
        
            errorArr1_B.append(error1_B)
            errorArr2_B.append(error2_B)
            errorArr3_B.append(error3_B)
            errorArr4_B.append(error4_B)    
        
        np.savetxt('error_public_A_%d_smoothened.dat'%year, np.c_[errorArr1_A, errorArr2_A, errorArr3_A, errorArr4_A], header="errorA1 errorA2 errorA3 errorA4")        
        np.savetxt("lambdaA_public_%d_smoothened.dat"%year, np.c_[theta1Arr_A], header="lambA")
        np.savetxt('error_public_B_%d_smoothened.dat'%year, np.c_[errorArr1_B, errorArr2_B, errorArr3_B, errorArr4_B], header="errorB1 errorB2 errorB3 errorB4")        
        np.savetxt("lambdaB_public_%d_smoothened.dat"%year, np.c_[theta1Arr_B], header="lambB")
        
democrats_bins = 0.5*(democrats_bins+np.roll(democrats_bins, -1))[:-1]      
republicans_bins = 0.5*(republicans_bins+np.roll(republicans_bins, -1))[:-1]   

lambA_mean_arr = []
lambB_mean_arr = []
lambA_std_arr = []
lambB_std_arr = []

for year in years:
    data = np.loadtxt("lambdaA_lambdaB_public_%d.dat"%year)    
    lambA_mean_arr.append(np.mean(data[:,0]))
    lambB_mean_arr.append(np.mean(data[:,1]))
    lambA_std_arr.append(np.std(data[:,0]))
    lambB_std_arr.append(np.std(data[:,1]))

plt.figure()
plt.title("U.S.~general public")      
plt.errorbar(years, lambA_mean_arr, yerr=lambA_std_arr, linewidth=0.5, color='blue', marker='o', markersize=5, alpha=0.5, label=r'$\lambda_A$')
plt.errorbar(years, lambB_mean_arr, yerr=lambB_std_arr, linewidth=0.5, color='red', marker='o', markersize=5, alpha=0.5, label=r'$\lambda_B$')
plt.xlim([1990, 2020])
plt.xlabel(r'Year')
plt.ylabel(r'Activist influence')
plt.legend(loc=2, frameon=False)
plt.ylim([0.8,1.4])
plt.tight_layout()
plt.savefig('lambdaA_lambdaB_public_smoothened.pdf')
plt.show()

lambdA = lambA_arr[-1]
lambdB = lambB_arr[-1]

xx = np.linspace(-1, 1, len(pA_arr_opt))

print(lambdA)
print(lambdB)
data = np.loadtxt('optimal_pqApqB_1994_public.dat')

pA_arr_opt = np.copy(data[:,1])
qA_arr_opt = np.copy(data[:,2])
pB_arr_opt = np.copy(data[:,3])
qB_arr_opt = np.copy(data[:,4])

pq = np.append(np.copy(pA_arr_opt), np.copy(qA_arr_opt))

pA_arr_opt /= lambdA**0.5
qA_arr_opt *= lambdA**0.5
pB_arr_opt *= lambdB**0.5
qB_arr_opt /= lambdB**0.5

plt.figure()
plt.plot(xx, pA_arr_opt, label=r'$p^A(x)$', color='k', ls='-', linewidth=1.)
plt.plot(xx, qA_arr_opt, label=r'$q^A(x)$', color='k', ls='--', linewidth=1.)
plt.plot(xx, pB_arr_opt, label=r'$p^B(x)$', color='grey', ls='-', linewidth=1.)
plt.plot(xx, qB_arr_opt, label=r'$q^B(x)$', color='grey', ls='--', linewidth=1.)
plt.xlabel(r'Ideological position')
plt.ylabel(r'Transition probability')
plt.legend(frameon=False, loc=8, ncol=2, fontsize=8)
plt.tight_layout()
plt.savefig('transition_probs_%d_public_smoothened.pdf'%yearyear)
plt.show()

stationaryA = product(pA_arr_opt, qA_arr_opt)

stationaryB = product(pB_arr_opt, qB_arr_opt)

plt.figure()
plt.title("U.S.~general public (%d)"%year)      
plt.plot(xx, stationaryA, linewidth=3., color='Grey', alpha=0.6)
plt.plot(democrats_bins, democrats_histogram, 'bo', zorder=10)
plt.plot(republicans_bins, republicans_histogram, 'ro', zorder=10)
plt.plot(10, 10, 'o', color='red', markerfacecoloralt='blue', fillstyle='right', label='Data (Dem./Rep.)')
plt.plot(xx, stationaryB, linewidth=3., color='Grey', alpha=0.6, label='Simulation')
plt.legend(loc=1, frameon=False, ncol=2, fontsize=8)
plt.xlabel(r'Ideological position')
plt.ylabel(r'PDF')
plt.xlim([-1,1])
plt.ylim([0, 1.6])
plt.tight_layout()
plt.savefig('ideology_distr_%d_public_smoothened.pdf'%yearyear)
plt.show()
#%%






plt.figure()
plt.plot(errorArr1, linewidth=2.)
plt.plot(errorArr2, linewidth=2.)
plt.plot(errorArr3, linewidth=2.)
plt.plot(errorArr4, linewidth=2.)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('iterations')
plt.ylabel('error')
plt.tight_layout()
plt.savefig('error.png')

thetaArr = np.asarray(theta1Arr)

np.savetxt('thetaArr.dat', np.c_[thetaArr[-10000::1,0], thetaArr[-10000::1,1], thetaArr[-10000::1,2], thetaArr[-10000::1,3]], header='beta gamma p r0')

plt.figure(figsize = fig_size)
#plt.plot(thetaArr[:,0])
plt.hist(thetaArr[-10000::1,0], bins=40)
plt.xlabel(r'$\beta$')
plt.ylabel(r'PDF')
plt.tight_layout()
plt.savefig('PDFbeta2015.png')


